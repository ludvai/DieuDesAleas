import dotenv
from twitchio.ext import commands
import os
from random import randint

dotenv.load_dotenv()

bot = commands.Bot(
    token=os.environ['TOKEN'],
    client_secret=os.environ['SECRET_KEY'],
    client_id=os.environ['CLIENT_ID'],
    nick=os.environ['BOT_NICK'],
    prefix=os.environ['BOT_PREFIX'],
    initial_channels=[os.environ['CHANNEL']]
)

DIE_FACES = 100

@bot.command(name='roll')
async def roll(ctx):
    roll_result = randint(1, DIE_FACES)
    await ctx.send(f"@{ctx.author.name} a obtenu un {roll_result}")

if __name__ == "__main__":
    bot.run()